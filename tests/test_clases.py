import unittest
import os
from sys import path
path.append(os.path.abspath(os.path.dirname(__file__))[:-5])
from src.clases import *


class TestTipoDeMonstruo(unittest.TestCase):
    def setUp(self):
        self.tipo_monstruo_agua = TipoDeMonstruo(TipoFuego, TipoTierra, "agua")
        
    def test_ataque_efectivo(self):
        self.assertEqual(self.tipo_monstruo_agua.atacar(TipoFuego(),True),18)
        self.assertEqual(self.tipo_monstruo_agua.atacar(TipoFuego(),False),12)
        self.assertEqual(self.tipo_monstruo_agua.__str__(),"agua")
        
    def test_ataque_inefectivo(self):
        self.assertEqual(self.tipo_monstruo_agua.atacar(TipoTierra(),True),12)
        self.assertEqual(self.tipo_monstruo_agua.atacar(TipoTierra(),False),8)
        self.assertEqual(self.tipo_monstruo_agua.__str__(), "agua")


class TestMonstruo(unittest.TestCase):
    def setUp(self):
        self.tipoFuego = TipoFuego()
        self.tipoTierra = TipoTierra()
        self.monstruo = Monstruo(self.tipoFuego, self.tipoTierra)
        self.monstruoDefensor = Monstruo(TipoAgua())

    def test_metodos_monstruo(self):
        self.assertEqual(self.tipoFuego, self.monstruo.get_tipo1())
        self.assertEqual(self.tipoTierra, self.monstruo.get_tipo2())
        self.assertEqual(self.monstruo.get_puntos_de_vida(), 100)
        self.assertEqual(self.monstruoDefensor.get_puntos_de_vida(), 100)
        self.assertEqual(self.monstruo.get_ataques_especiales(), 4)
        self.assertEqual(self.monstruoDefensor.get_ataques_especiales(), 4)

    def test_danio_vida(self):
        self.monstruo.defender(10)
        self.assertEqual(self.monstruo.get_puntos_de_vida(), 90)
        self.assertEqual(self.monstruo.atacar(1, self.monstruoDefensor), 8)
        self.assertEqual(self.monstruo.atacar(2, self.monstruoDefensor), 12)
        self.assertEqual(self.monstruo.atacar(3, self.monstruoDefensor), 12)
        self.assertEqual(self.monstruo.atacar(4, self.monstruoDefensor), 18)
        self.assertEqual(self.monstruo.get_ataques_especiales(), 2)

    def atacar_especiales_sin_especiales(self):
        self.monstruo.ataques_especiales = 0
        self.assertEqual(self.monstruo.get_ataques_especiales(), 0)
        self.assertRaises(SinAtaquesEspeciales(), self.monstruo.atacar(4, self.monstruoDefensor))

    def monstruo_derrotado_test_case(self):
        self.monstruoDefensor.puntos_de_vida = 1
        self.assertRaises(MonstruoDerrotado(), self.monstruo.atacar(1, self.monstruoDefensor))


class TestTirarDado(unittest.TestCase):
    def setUp(self):
        self.dadoTirado = tirar_dado()

    def test_tirada_dado(self):
        self.assertTrue(self.dadoTirado > 0)
        self.assertTrue(self.dadoTirado < 7)


class TestJugador(unittest.TestCase):
    def setUp(self):
        self.jugador1 = Jugador("emiliano")
        self.jugador2 = Jugador("oscar")
        self.monstruo1 = Monstruo(TipoFuego(), TipoTierra())
        self.monstruo2 = Monstruo(TipoAgua())
        self.jugador1.asignar_monstruo(self.monstruo1)
        self.jugador2.asignar_monstruo(self.monstruo2)

    def test_getters(self):
        self.assertEqual("emiliano", self.jugador1.get_nombre())
        self.assertEqual("oscar", self.jugador2.get_nombre())
        self.assertEqual(self.monstruo1, self.jugador1.get_monstruo())
        self.assertEqual(self.monstruo2, self.jugador2.get_monstruo())

    def test_resumen(self):
        self.assertEqual(self.jugador1.producir_resumen(),
		'Jugador: emiliano\nMonstruo tipo Fuego y tipo Tierra\nPuntos de vida restantes: 100\nAtaques especiales restantes: 4')
        self.assertEqual(self.jugador2.producir_resumen(),
		'Jugador: oscar\nMonstruo tipo Agua\nPuntos de vida restantes: 100\nAtaques especiales restantes: 4')


class BatallaDeMonstruosTestCase(unittest.TestCase):
    def setUp(self):
        self.batalla = BatallaDeMonstruos("emiliano", "oscar", True)
        self.tipoFuego = TipoFuego()
        self.tipoTierra = TipoTierra()
        self.tipoAgua = TipoAgua()
        self.batalla.crear_monstruo(self.tipoFuego, self.tipoTierra)
        self.batalla.crear_monstruo(self.tipoAgua, None)
        self.batalla.jugador_ataca(4)
        self.batalla._cambiar_turno_()

    def test_getters_jugador_actual(self):
        self.assertEqual(self.batalla.get_jugador_actual().get_nombre(), "emiliano")
        self.assertEqual(self.batalla.get_jugador_actual().get_monstruo().get_tipo1(), self.tipoFuego)
        self.assertEqual(self.batalla.get_jugador_actual().get_monstruo().get_tipo2(), self.tipoTierra)
        self.assertEqual(self.batalla.get_jugador_actual().get_monstruo().get_puntos_de_vida(), 100)
        self.assertEqual(self.batalla.juego_comenzo(), True)

    def test_getters_jugador_no_actual(self):
        self.assertEqual(self.batalla.get_jugador_no_actual().get_nombre(), "oscar")
        self.assertEqual(self.batalla.get_jugador_no_actual().get_monstruo().get_tipo1(), self.tipoAgua)
        self.assertEqual(self.batalla.get_jugador_no_actual().get_monstruo().get_puntos_de_vida(), 82)
        self.assertEqual(self.batalla.juego_comenzo(), True)

class TestTirarDado(unittest.TestCase):
    def setUp(self):
        self.dadoTirado = tirar_dado()

    def test_tirada_dado(self):
        self.assertTrue(self.dadoTirado > 0)
        self.assertTrue(self.dadoTirado < 7)

    def test_tirada_dado_nunca_fuera_de_rango(self):
        self.assertFalse(self.dadoTirado <= 0)
        self.assertFalse(self.dadoTirado >= 7)


class TestBatallaDeMonstruos(unittest.TestCase):
    def setUp(self):
        self.batalla = BatallaDeMonstruos("emiliano", "oscar", True)

    def test_costructor(self):
        self.assertEqual(self.batalla.jugadores[0].get_nombre(),"emiliano")
        self.assertEqual(self.batalla.jugadores[1].get_nombre(), "oscar")
        self.assertEqual(self.batalla.juego_iniciado, False)
        self.assertEqual(self.batalla.jugador_actual, 0)

    def test_crear_monstruo_jugador1(self):
        self.tipoFuego = TipoFuego()
        self.tipoTierra = TipoTierra()
        self.batalla.crear_monstruo(self.tipoFuego, self.tipoTierra)
        self.assertNotEqual(self.batalla.get_jugador_no_actual().get_monstruo(), None)
        self.assertEqual(self.batalla.get_jugador_actual().get_monstruo(), None)

    def test_crear_monstruo_jugador2(self):
        self.tipoFuego = TipoFuego()
        self.tipoTierra = TipoTierra()
        self.batalla._cambiar_turno_()
        self.batalla.crear_monstruo(self.tipoFuego, self.tipoTierra)
        self.assertNotEqual(self.batalla.get_jugador_no_actual().get_monstruo(), None)
        self.assertEqual(self.batalla.get_jugador_actual().get_monstruo(), None)

    def test_crear_monstruo_jugador1_mismo_tipo_excepcion(self):
        self.tipo1 = TipoFuego()
        self.tipo2 = TipoFuego()
        self.assertRaises(TiposIguales, lambda: self.batalla.crear_monstruo(self.tipo1, self.tipo2))

    def test_jugador1_ataca_jugador2(self):
        self.tipoFuego = TipoFuego()
        self.tipoTierra = TipoTierra()
        self.batalla.crear_monstruo(self.tipoFuego, self.tipoTierra)
        self.batalla.crear_monstruo(self.tipoTierra, self.tipoFuego)
        self.batalla.jugador_ataca(1)
        self.assertEqual(self.batalla.get_jugador_no_actual().get_monstruo().get_puntos_de_vida(), 100)

    def test_jugador2_ataca_jugador1(self):
        self.batalla._cambiar_turno_()
        self.tipoFuego = TipoFuego()
        self.tipoTierra = TipoTierra()
        self.batalla.crear_monstruo(self.tipoFuego, self.tipoTierra)
        self.batalla.crear_monstruo(self.tipoTierra, self.tipoFuego)
        self.batalla.jugador_ataca(1)
        self.assertEqual(self.batalla.get_jugador_no_actual().get_monstruo().get_puntos_de_vida(), 100)

    def test_jugador1_derrota_jugador2(self):
        self.tipoFuego = TipoFuego()
        self.tipoAgua = TipoAgua()
        self.batalla.crear_monstruo(self.tipoAgua, self.tipoFuego)
        self.batalla.crear_monstruo(self.tipoFuego,self.tipoAgua)
        self.batalla.get_jugador_no_actual().get_monstruo().puntos_de_vida = 0
        self.assertRaises(MonstruoDerrotado, lambda: self.batalla.jugador_ataca(1))

    def test_jugador2_derrota_jugador1(self):
        self.batalla._cambiar_turno_()
        self.tipoFuego = TipoFuego()
        self.tipoTierra = TipoTierra()
        self.batalla.crear_monstruo(self.tipoFuego, self.tipoTierra)
        self.batalla.crear_monstruo(self.tipoTierra, self.tipoFuego)
        self.batalla.get_jugador_no_actual().get_monstruo().puntos_de_vida = 0
        self.assertRaises(MonstruoDerrotado, lambda: self.batalla.jugador_ataca(1))

    def test_cambiar_turno(self):
        self.batalla._cambiar_turno_()
        self.assertEqual(self.batalla.jugador_actual,1)

    def test_get_jugador_actual(self):
        self.assertEqual(self.batalla.get_jugador_actual(), self.batalla.jugadores[0])

    def test_get_jugador_no_actual(self):
        self.assertEqual(self.batalla.get_jugador_no_actual(), self.batalla.jugadores[1])

    def test_juego_no_comenzo(self):
        self.assertEqual(self.batalla.juego_comenzo(),False)

    def test_juego_comenzo(self):
        self.tipoFuego = TipoFuego()
        self.tipoAgua = TipoAgua()
        self.batalla.crear_monstruo(self.tipoAgua, self.tipoFuego)
        self.batalla.crear_monstruo(self.tipoFuego,self.tipoAgua)
        self.assertEqual(self.batalla.juego_comenzo(),True)

    def test_termino_el_juego(self):
        self.tipoFuego = TipoFuego()
        self.tipoAgua = TipoAgua()
        self.batalla.crear_monstruo(self.tipoAgua, self.tipoFuego)
        self.batalla.crear_monstruo(self.tipoFuego,self.tipoAgua)
        self.assertEqual(self.batalla.juego_termino(),False)

    def test_termino_el_juego(self):
        self.tipoFuego = TipoFuego()
        self.tipoAgua = TipoAgua()
        self.batalla.crear_monstruo(self.tipoAgua, self.tipoFuego)
        self.batalla.crear_monstruo(self.tipoFuego,self.tipoAgua)
        self.batalla.get_jugador_no_actual().get_monstruo().puntos_de_vida = 0
        self.assertEqual(self.batalla.juego_termino(),True)


class TestJugador(unittest.TestCase):

    def setUp(self):
        self.jugador1 = Jugador("emiliano")
        self.jugador2 = Jugador("oscar")

    def test_constructor(self):
        self.assertEqual(self.jugador1.nombre, "emiliano")
        self.assertEqual(self.jugador1.monstruo, None)
        self.assertEqual(self.jugador2.nombre, "oscar")
        self.assertEqual(self.jugador2.monstruo, None)

    def test_asignar_monstruo(self):
        self.tipoFuego = TipoFuego()
        self.tipoAgua = TipoAgua()
        monstruo = Monstruo(self.tipoAgua, self.tipoFuego)
        self.jugador1.asignar_monstruo(monstruo)
        self.assertEqual(self.jugador1.monstruo, monstruo)
        self.jugador2.asignar_monstruo(monstruo)
        self.assertEqual(self.jugador2.monstruo, monstruo)

    def test_get_nombre(self):
        self.assertEqual(self.jugador1.get_nombre(), "emiliano")
        self.assertEqual(self.jugador2.get_nombre(), "oscar")

    def test_get_monstrouo(self):
        self.tipoFuego = TipoFuego()
        self.tipoAgua = TipoAgua()
        monstruo = Monstruo(self.tipoAgua, self.tipoFuego)
        self.jugador1.asignar_monstruo(monstruo)
        self.assertEqual(self.jugador1.get_monstruo(), monstruo)
        self.jugador2.asignar_monstruo(monstruo)
        self.assertEqual(self.jugador2.get_monstruo(), monstruo)

    def test_producir_resumen(self):
        self.tipoFuego = TipoFuego()
        self.tipoAgua = TipoAgua()
        monstruo = Monstruo(self.tipoAgua, self.tipoFuego)
        self.jugador1.asignar_monstruo(monstruo)
        self.jugador2.asignar_monstruo(monstruo)
        self.assertEqual(self.jugador1.producir_resumen(),
                             'Jugador: emiliano\nMonstruo tipo Agua y tipo Fuego\nPuntos de vida restantes: 100\nAtaques especiales restantes: 4')
        self.assertEqual(self.jugador2.producir_resumen(),
                             'Jugador: oscar\nMonstruo tipo Agua y tipo Fuego\nPuntos de vida restantes: 100\nAtaques especiales restantes: 4')


class TestTipoAgua(unittest.TestCase):
    def setUp(self):
        self.tipoAgua = TipoAgua()

    def test_clase_agua(self):
        self.assertEqual(self.tipoAgua.tipo, "Agua")


class TestTipoFuego(unittest.TestCase):
    def setUp(self):
        self.tipoFuego = TipoFuego()

    def test_clase_fuego(self):
        self.assertEqual(self.tipoFuego.tipo, "Fuego")


class TestTipoTierra(unittest.TestCase):
    def setUp(self):
        self.tipoTierra = TipoTierra()

    def test_clase_tierra(self):
        self.assertEqual(self.tipoTierra.tipo, "Tierra")


class TestTipoAire(unittest.TestCase):
    def setUp(self):
        self.tipoAire = TipoAire()

    def test_clase_aire(self):
        self.assertEqual(self.tipoAire.tipo, "Aire")

class TestTipoDeMonstruo(unittest.TestCase):
    def setUp(self):
        self.tipoTierra = TipoTierra()
        self.tipoAire = TipoAire()
        self.tipoAgua = TipoAgua()
        self.tipo = TipoDeMonstruo(TipoAire, TipoAgua, 'Fuego')

    def test_toString(self):
        self.assertEqual(self.tipo.__str__(), "Fuego")

    def test_constructor(self):
        self.assertEqual(self.tipo.tipo, "Fuego")

    def test_atacar_normal(self):
        self.assertEqual(self.tipo.atacar(self.tipoTierra, False),10)

    def test_atacar_especial(self):
        self.assertEqual(self.tipo.atacar(self.tipoTierra, True),15)

    def test_atacar_normal_eficiencia(self):
        self.assertEqual(self.tipo.atacar(self.tipoAire, False),12)

    def test_atacar_normal_resistencia(self):
        self.assertEqual(self.tipo.atacar(self.tipoAgua, False),8)

    def test_atacar_especial_eficiencia(self):
        self.assertEqual(self.tipo.atacar(self.tipoAire, True),18)

    def test_atacar_especial_resistencia(self):
        self.assertEqual(self.tipo.atacar(self.tipoAgua, True),12)


class TestClaseMonstruo(unittest.TestCase):
    def setUp(self):
        self.tipoAire = TipoAire()
        self.tipoAgua = TipoAgua()
        self.monstruo = Monstruo(self.tipoAire, self.tipoAgua)
        self.tipoFuego = TipoFuego()
        self.tipoTierra = TipoTierra()
        self.monstruo_defensor = Monstruo(self.tipoTierra, self.tipoFuego)

    def test_constructor(self):
        self.assertEqual(self.monstruo.tipos[0], self.tipoAire)
        self.assertEqual(self.monstruo.tipos[1], self.tipoAgua)
        self.assertEqual(self.monstruo.ataques_especiales, 4)
        self.assertEqual(self.monstruo.puntos_de_vida,100)

    def test_get_ataques_especiales(self):
        self.assertEqual(self.monstruo.get_ataques_especiales(), 4)

    def test_get_tipo1(self):
        self.assertEqual(self.monstruo.get_tipo1(), self.tipoAire)

    def test_get_tipo2(self):
        self.assertEqual(self.monstruo.get_tipo2(), self.tipoAgua)

    def test_puntos_de_vida(self):
        self.assertEqual(self.monstruo.get_puntos_de_vida(), 100)

    def test_defender(self):
        self.monstruo.defender(10)
        self.assertEqual(self.monstruo.get_puntos_de_vida(), 90)

    def test_defender_con_exception(self):
        self.monstruo.puntos_de_vida=0
        self.assertRaises(MonstruoDerrotado, lambda: self.monstruo.defender(10))

    def test_atacar1(self):
        self.assertEqual(self.monstruo.atacar(1,self.monstruo_defensor),10)

    def test_atacar2(self):
        self.assertEqual(self.monstruo.atacar(2,self.monstruo_defensor),15)

    def test_atacar3(self):
        self.assertEqual(self.monstruo.atacar(3,self.monstruo_defensor),10)

    def test_atacar4(self):
        self.assertEqual(self.monstruo.atacar(4,self.monstruo_defensor),15)

    def test_atacar_exception(self):
        self.monstruo.ataques_especiales=0
        self.assertRaises(SinAtaquesEspeciales, lambda : self.monstruo.atacar(4, self.monstruo_defensor))
        self.assertRaises(SinAtaquesEspeciales, lambda : self.monstruo.atacar(4, self.monstruo_defensor))

if __name__ == "__main__":
    unittest.main()