import random
from sys import path
from sys import exit
import os
path.append(os.path.abspath(os.path.dirname(__file__)))
from clases import *
from administrador_partidas import *

# Las cadenas de guiones impresas funcionan a modo de "renglones", permitiendo mejor
# legibilidad para el usuario en la consola

def mostrar_ayuda():
	print('En Batalla de Monstruos, dos jugadores se enfrentan con monstruos personalizados.'+
	' Cada jugador tiene un monstruo, y cada monstruo posee uno o dos tipos (puede elegir dos '+
	'veces el mismo para duplicar resistencias y debilidades), a eleccion del '+
	'jugador. Los tipos posibles de monstruo son:\n\nFuego: debil a los tipo agua y resistente '+
	'a los tipo aire, causa mas dano a los tipo aire.\nAgua: debil a los tipo tierra y '+
	'resistente a los tipo fuego, causa mas dano a los tipo fuego.\nTierra: debil a los tipo '+
	'aire y resistente a los tipo agua, causa mas dano a los tipo agua.\nAire: debil a los '+
	'tipo fuego y resistente a los tipo tierra, causa mas dano a los tipo tierra.\n\nEl juego va '+
	'por turnos durante los cuales el jugador correspondiente puede elegir con que tipo '+
	'atacar, y si realizar un ataque simple (10 de dano) o especial (15 de dano). Cada monstruo '+
	'empieza con 4 ataques especiales y 100 puntos de vida y una vez '+
	'que estos llegan a 0 (cero), el monstruo se desmaya y la batalla termina. Por ultimo, es '+
	'posible guardar la partida actual en cualquier momento del juego, para retomarla mas '+
	'tarde en el mismo punto en que dejaron de jugar.\n\nPuedes escribir "ayuda" en cualquier '+
	'momento para ver los comandos disponibles en ese momento.\n')

def crear_tipo1():
	while True:
		comando = input()
		if comando == '1':
			print('Eligio tipo fuego\n')
			print('-----------------------------------------------------------------')
			return TipoFuego()
		elif comando == '2':
			print('Eligio tipo tierra\n')
			print('-----------------------------------------------------------------')
			return TipoTierra()
		elif comando == '3':
			print('Eligio tipo aire\n')
			print('-----------------------------------------------------------------')
			return TipoAire()
		elif comando == '4':
			print('Eligio tipo agua\n')
			print('-----------------------------------------------------------------')
			return TipoAgua()
		elif comando == 'ayuda':
			print('Elige el primer tipo de tu monstruo escribiendo el numero correspondiente:'+
			'\n1. Fuego\n2. Tierra\n3. Aire\n4. Agua\n\nOtros comandos:\n5. Cerrar el juego')
			print('-----------------------------------------------------------------')
			continue
		elif comando == '5':
			print('Cerrando juego.')
			exit()
		else:
			print('No es un comando valido\n')
			print('-----------------------------------------------------------------')

def crear_tipo2():
	while True:
		comando = input()
		if comando == '1':
			print('Eligio tipo fuego\n')
			print('-----------------------------------------------------------------')
			return TipoFuego()
		elif comando == '2':
			print('Eligio tipo tierra\n')
			print('-----------------------------------------------------------------')
			return TipoTierra()
		elif comando == '3':
			print('Eligio tipo aire\n')
			print('-----------------------------------------------------------------')
			return TipoAire()
		elif comando == '4':
			print('Eligio tipo agua\n')
			print('-----------------------------------------------------------------')
			return TipoAgua()
		elif comando == '5':
			print('Eligio sin segundo tipo\n')
			print('-----------------------------------------------------------------')
			return None
		elif comando == 'ayuda':
			print('Elige el primer tipo de tu monstruo escribiendo el numero correspondiente:'+
			'\n1. Fuego\n2. Tierra\n3. Aire\n4. Agua\n5. Sin segundo tipo\n\nOtros comandos:'+
			'6. Cerrar el juego\n')
			print('-----------------------------------------------------------------')
			continue
		elif comando == '6':
			print('Cerrando juego.')
			exit()
		else:
			print('No es un comando valido\n')

def atacar(juego):
	while True:
		try:
			if juego.get_jugador_actual().get_monstruo().get_tipo2() and\
			type(juego.get_jugador_actual().get_monstruo().get_tipo2()) !=\
			type(juego.get_jugador_actual().get_monstruo().get_tipo1()):					#El monstruo atacante tiene dos tipos

				if juego.get_jugador_actual().get_monstruo().get_ataques_especiales() > 0:
					print('Elija un ataque:\n1. ', juego.get_jugador_actual().get_monstruo().get_tipo1().__str__(),
					'simple\n2. ', juego.get_jugador_actual().get_monstruo().get_tipo1().__str__(), 'especial.\n3. ',
					juego.get_jugador_actual().get_monstruo().get_tipo2().__str__(), 'simple\n4. ',
					juego.get_jugador_actual().get_monstruo().get_tipo2().__str__(), 'especial',
					'\nEscriba ayuda para ver comandos adicionales.\n')

					comando = input('Numero de ataque: ')
					if comando == 'ayuda':
						print('Comandos adicionales:\n5. Cerrar el juego\n6. Cancelar el ataque\n7. Mostrar una descripcion del juego')
						print('-----------------------------------------------------------------')
					elif comando == '1' or comando == '2' or comando == '3' or comando == '4':
						print('Dano causado:' + str(juego.jugador_ataca(int(comando))) + '\nVida restante:',
						str(juego.get_jugador_actual().get_monstruo().get_puntos_de_vida()))
						print('-----------------------------------------------------------------')
						break
					elif comando == '5':
						print('Cerrando el juego...')
						exit()
					elif comando == '6':
						print('Cancelando ataque...')
						break
					elif comando == '7':
						mostrar_ayuda()
						print('-----------------------------------------------------------------')
					else:
						print('Comando invalido')
						print('-----------------------------------------------------------------')

				else:	#No tiene ataques especiales
					print('Elija un ataque:\n1.', juego.get_jugador_actual().get_monstruo().get_tipo1().__str__(),
					'simple\n3.', juego.get_jugador_actual().get_monstruo().get_tipo2().__str__(), 'simple',
					'\nYa no lo quedan ataques especiales. Escriba ayuda para ver comandos adicionales.')

					comando = input('Numero de ataque: ')
					if comando == 'ayuda':
						print('Comandos adicionales:\n5. Cerrar el juego\n6. Cancelar el ataque\n7. Mostrar una descripcion del juego')
						print('-----------------------------------------------------------------')
					elif comando == '1' or comando == '3':
						print('Dano causado:' + str(juego.jugador_ataca(int(comando))) + '\nVida restante:',
						str(juego.get_jugador_no_actual().get_monstruo().get_puntos_de_vida()))
						print('-----------------------------------------------------------------')
						break
					elif comando == '5':
						print('Cerrando el juego...')
						exit()
					elif comando == '6':
						print('Cancelando ataque...')
						break
					elif comando == '7':
						mostrar_ayuda()
						print('-----------------------------------------------------------------')
					else:
						print('Comando invalido')
						print('-----------------------------------------------------------------')
			else:	#El monstruo atacante tiene un solo tipo o dos veces el mismo
				if juego.get_jugador_actual().get_monstruo().get_ataques_especiales() > 0:
					print('Elige un ataque:\n1.', juego.get_jugador_actual().get_monstruo().get_tipo1().__str__(),
					'simple\n2.', juego.get_jugador_actual().get_monstruo().get_tipo1().__str__(),
					'especial\nEscriba ayuda para ver comandos adicionales.')

					comando = input('Numero de ataque: ')
					if comando == 'ayuda':
						print('Comandos adicionales:\n3. Cerrar el juego\n4. Cancelar el ataque\n5. Mostrar una descripcion del juego')
						print('-----------------------------------------------------------------')
					elif comando == '1' or comando == '2':
						print('Dano causado:' + str(juego.jugador_ataca(int(comando))) + '\nVida restante:',
						str(juego.get_jugador_no_actual().get_monstruo().get_puntos_de_vida()))
						print('-----------------------------------------------------------------')
						break
					elif comando == '3':
						print('Cerrando el juego...')
						exit()
					elif comando == '4':
						print('Cancelando ataque...')
						break
					elif comando == '5':
						mostrar_ayuda()
						print('-----------------------------------------------------------------')
					else:
						print('Comando invalido')
				else:	#No tiene ataques especiales
					print('Solo tiene un ataque, se le acabaron los ataques especiales:\n1.',
					juego.get_jugador_actual().get_monstruo().get_tipo1().__str__(),
					'simple.\nYa no le quedan ataques especiales. Escriba ayuda para ver comandos adicionales.')

					comando = input('Numero de ataque: ')
					if comando == 'ayuda':
						print('Comandos adicionales:\n3. Cerrar el juego\n4. Cancelar el ataque\n5. Mostrar una descripcion del juego')
						print('-----------------------------------------------------------------')
					elif comando == '1':
						print('Dano causado:' + str(juego.jugador_ataca(int(comando))) + '\nVida restante:',
						str(juego.get_jugador_no_actual().get_monstruo().get_puntos_de_vida()))
						print('-----------------------------------------------------------------')
						break
					elif comando == '3':
						print('Cerrando el juego...')
						exit()
					elif comando == '4':
						print('Cancelando ataque...')
						break
					elif comando == '5':
						mostrar_ayuda()
						print('-----------------------------------------------------------------')
					else:
						print('Comando invalido')
		except MonstruoDerrotado:
			print('No se puede atacar, la partida ya acabó')

def crear_partida():
	nombre1 = input('Nombre del jugador 1: ')
	nombre2 = input('Nombre del jugador 2: ')
	juego = None

	input(nombre1 + ' presiona enter para tirar el dado:')
	dado1 = tirar_dado()
	print(dado1)

	input(nombre2 + ' presiona enter para tirar el dado:')
	dado2 = tirar_dado()
	print(dado2)
	print('-----------------------------------------------------------------')

	if dado1 > dado2:
		print('Comienza ' + nombre1)
		juego = BatallaDeMonstruos(nombre1, nombre2, True)
	else:
		print('Comienza ' + nombre2)
		juego = BatallaDeMonstruos(nombre1, nombre2, False)

	while not juego.juego_comenzo():
		print('Elige el primer tipo de tu monstruo escribiendo el numero correspondiente:'+
		'\n1. Fuego\n2. Tierra\n3. Aire\n4. Agua')
		tipo1 = crear_tipo1()

		print('Elige el segundo tipo de tu monstruo escribiendo el numero correspondiente:'+
		'\n1. Fuego\n2. Tierra\n3. Aire\n4. Agua\n5. Sin segundo tipo')
		tipo2 = crear_tipo2()

		try:
			juego.crear_monstruo(tipo1, tipo2)
			if not juego.juego_comenzo():
				print('Ahora elige el jugador ' + juego.get_jugador_actual().get_nombre())
			print('-----------------------------------------------------------------')
		except TiposIguales:
			print('No se puede elegir dos veces el mismo tipo')

	return juego

if __name__=="__main__":

	while True:
		juego = None
		print('Bienvenido a Batalla de Monstruos. Puedes escribir "ayuda" para ver una lista de '+
		'comandos disponibles en cualquier momento del juego, menos durante la seleccion de nombres.\n')
		print('Escribe el numero correspondiente:\n1. Crear partida nueva\n2. Cargar partida guardada\n3.'+
		' Mostrar descripcion del juego\n4. Cerrar el juego\n5. Borrar todas las partidas guardadas')
		print('-----------------------------------------------------------------')
		while True:
			comando = input('Comando: ')

			if comando == 'ayuda': #Mostrar comandos disponibles
				print('1. Crear partida nueva\n2. Cargar partida guardada\n3. Mostrar descripcion'+
				'del juego\n4. Cerrar el juego\n5. Borrar todas las partidas guardadas')
				print('-----------------------------------------------------------------')

			elif comando == '1': #Crear partida nueva
				juego = crear_partida()
				break

			elif comando == '2': #Cargar partida existente
				juego = cargar_partida(juego)
				if juego != None:
					break
				else:
					print('Bienvenido a Batalla de Monstruos. Puedes escribir -ayuda para ver una lista de '+
					'comandos disponibles en cualquier momento.')
					print('Escribe el numero correspondiente:\n1. Crear partida nueva\n2. Cargar partida guardada\n3.'+
					' Mostrar descripcion del juego\n4. Cerrar el juego')
					print('-----------------------------------------------------------------')

			elif comando == '3': #Mostrar descripcion del juego
				mostrar_ayuda()
				print('-----------------------------------------------------------------')

			elif comando == '4': #Cerrar el juego
				print('Cerrando el juego.')
				exit()
			elif comando == '5': #Borrar las partidas guardadas
				erase()
				print('Partidas borradas')
				print('-----------------------------------------------------------------')
			else:
				print('Comando invalido\n')
				print('-----------------------------------------------------------------')

		while True:
			if not juego.juego_termino():
				print('Turno de ' + juego.get_jugador_actual().get_nombre() + '. Escribe el numero '+
					'correspondiente:\n1. Atacar\n2. Guardar la partida\n3. Cerrar el juego\n4. Volver'+
					' al menu inicial\n5. Obtener un resumen de la partida\n6. Cargar una partida '+
					'existente\n7. Ver una descripcion del juego')

				comando = input('Comando: ')

				if comando == 'ayuda': #Mostrar lista de comandos disponibles
					print('No hay mas comandos disponibles')
					print('-----------------------------------------------------------------')

				elif comando == '1': #Elegir ataque
					atacar(juego)

				elif comando == '2': #Guardar la partida
					comando = input('Nombre de la partida a guardar: ')
					save(juego, comando)
					print('Partida guardada')
					print('-----------------------------------------------------------------')

				elif comando == '3': #Cerrar el juego
					print('Cerrando juego.')
					exit()

				elif comando == '4': #Volver al menu inicial
					print('Volviendo al menu inicial\n')
					print('-----------------------------------------------------------------')
					break

				elif comando == '5': #Mostar un resumen del estado de la partida
					print(juego.get_jugador_actual().producir_resumen() + '\n')
					print(juego.get_jugador_no_actual().producir_resumen() + '\n')
					print('-----------------------------------------------------------------')

				elif comando == '6': #Cargar una partida
					juego = cargar_partida(juego)

				elif comando == '7': #Mostrar descripcion del juego
					mostrar_ayuda()
					print('-----------------------------------------------------------------')
				else:
					print('Comando invalido')
					print('-----------------------------------------------------------------')

			else:
				print('El juego termino, gano el jugador', juego.get_jugador_actual().get_nombre(),
				'\nEscribe 1 para volver al menu de inicio.')
				print('-----------------------------------------------------------------')
				comando = input()
				if comando == '1':
					print('Volviendo al menu inicial\n')
					break