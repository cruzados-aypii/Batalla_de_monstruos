import csv
import random

def tirar_dado():
	"""Retorna un valor entero entre 1 y 6, extremos inclusives.
	"""
	return random.randint(1, 6)

class BatallaDeMonstruos(object):
	"""Clase BatallaDeMonstruos. Representa el juego Batalla de Monstruos.
	
	Argumentos:
	nombre_jug1 -- nombre del jugador 1.
	nombre_jug2 -- nombre del jugador 2.
	empieza_jugador1 -- booleano que determina quién juega primero. Se recomienda usar
	la función tirar_dado().
	"""

	def __init__(self, nombre_jug1, nombre_jug2, empieza_jugador1):
		jugador1 = Jugador(nombre_jug1)
		jugador2 = Jugador(nombre_jug2)
		self.jugadores = (jugador1, jugador2)
		self.juego_iniciado = False
		self.jugador_actual = 0 if empieza_jugador1 else 1

	def crear_monstruo(self, tipo1, tipo2):
		"""Crea un monstruo con los tipos indicados para el jugador actual.
		"""
		if type(tipo1) == type(tipo2):
			raise TiposIguales('No se puede elegir dos veces el mismo tipo')
		self.get_jugador_actual().asignar_monstruo(Monstruo(tipo1, tipo2))
		self._cambiar_turno_()
		if self.get_jugador_actual().get_monstruo():
			self.juego_iniciado = True

	def jugador_ataca(self, tipo_ataque):
		"""El monstruo del jugador actual ataca al monstruo rival.
		"""
		if self.juego_iniciado:
			dano = self.get_jugador_actual().get_monstruo().atacar(tipo_ataque, self.\
			jugadores[self.jugador_actual - 1].get_monstruo())
			self._cambiar_turno_()
			return dano

	def _cambiar_turno_(self):
		"""Metodo interno, finaliza el turno del jugador actual.
		"""
		self.jugador_actual = 0 if self.jugador_actual == 1 else 1

	def get_jugador_actual(self):
		"""Retorna el jugador actual.
		"""
		return self.jugadores[self.jugador_actual]

	def get_jugador_no_actual(self):
		"""Retorna el jugador al cual no le toca jugar
		"""
		return self.jugadores[self.jugador_actual - 1]

	def juego_comenzo(self):
		"""Retorna True si el juego comenzo, de otra manera False.
		"""
		return self.juego_iniciado

	def juego_termino(self):
		"""Retorna True si el juego termino, False de otra manera.
		"""
		return self.jugadores[0].get_monstruo().get_puntos_de_vida() == 0 or\
		self.jugadores[1].get_monstruo().get_puntos_de_vida() == 0

class Jugador(object):
	"""Clase Jugador, representa al jugador, con su nombre y monstruo propios.
	"""

	def __init__(self, nombre):
		self.nombre = nombre
		self.monstruo = None

	def asignar_monstruo(self, monstruo):
		"""Asigna el monstruo designado al jugador.
		"""
		self.monstruo = monstruo

	def producir_resumen(self):
		"""Genera un resumen del jugador.
		"""
		resumen = ''
		if self.monstruo.get_tipo2():
			resumen = 'Jugador: ' + self.nombre + '\nMonstruo tipo ' + self.monstruo.get_tipo1().__str__() +\
			' y tipo ' + self.monstruo.get_tipo2().__str__() + '\nPuntos de vida restantes: ' + str(self.monstruo.
			get_puntos_de_vida()) + '\nAtaques especiales restantes: ' + str(self.monstruo.get_ataques_especiales())
		else:
			resumen = 'Jugador: ' + self.nombre + '\nMonstruo tipo ' + self.monstruo.get_tipo1().__str__() +\
			'\nPuntos de vida restantes: ' + str(self.monstruo.get_puntos_de_vida()) + '\nAtaques especiales restantes: '\
			+ str(self.monstruo.get_ataques_especiales())
		return resumen

	def get_nombre(self):
		"""Retorna el nombre del jugador.
		"""
		return self.nombre

	def get_monstruo(self):
		"""Retorna el monstruo del jugador.
		"""
		return self.monstruo

class Monstruo(object):
	"""Clase monstruo, representa un monstruo del juego Batalla
	de Monstruos. Define la manera en que los monstruos interactuan
	entre si.
	
	Argumentos:
	tipo1 -- el tipo 1 del Monstruo.
	tipo2 -- el tipo 2 del Monstruo (opcional). Valor predeterminado: None
	"""
	
	def __init__(self, tipo1, tipo2=None):
		self.tipos = (tipo1, tipo2)
		self.puntos_de_vida = 100
		self.ataques_especiales = 4
	
	def atacar(self, ataque, monstruo_def):
		"""Calcula el dano causado a un monstruo determinado.
		
		Argumentos:
		ataque -- indica el tipo de ataque elegido, entero entre 1 y 4:
			1. ataque simple de tipo get_tipo1()
			2. ataque especial de tipo get_tipo1()
			3. ataque simple de tipo get_tipo2()
			4. ataque especial de tipo get_tipo2()
		Si get_ataques_especiales() devuelve 0, los ataques 2 y 4 estaran 
		deshabilitados.
		monstruo_def -- el monstruo defensor
		
		Excepciones:
		MonstruoDerrotado -- Levantada cuando monstruo_def ya no tiene puntos
		de vida restantes.
		SinAtaquesEspeciales -- Levantada cuando ataque == 2 o ataque == 4 y 
		get_ataques_especiales devuelve 0.
		AttributeError -- Levantada cuando se intenta ataque == 3 o ataque == 4
		y get_tipo2 devuelve None.
		
		Retorna:
		El dano causado al monstruo defensor.
		"""
		tipo1_def = monstruo_def.get_tipo1()
		tipo2_def = monstruo_def.get_tipo2()
		dano = 0

		if ataque == 1 or ataque == 3:
			dano = self.tipos[int(ataque/3)].atacar(tipo1_def, False) + \
			self.tipos[int(ataque/3)].atacar(tipo2_def, False) - 10
		elif ataque == 2 or ataque == 4:
			if self.ataques_especiales == 0:
				raise SinAtaquesEspeciales("El monstruo no tiene mas ataques especiales.")
			dano = self.tipos[int(ataque/4)].atacar(tipo1_def, True) + \
			self.tipos[int(ataque/4)].atacar(tipo2_def, True) - 15
			self.ataques_especiales -= 1

		monstruo_def.defender(dano)
		return dano

	def defender(self, dano):
		"""Resta el dano recibido de los puntos de vida del monstruo.
		
		Argumentos:
		dano -- el dano recibido por el monstruo
		
		Excepciones:
		MonstruoDerrotado -- levantada cuando el Monstruo ya no tiene
		puntos de vida restantes.
		"""
		if self.puntos_de_vida > 0:
			self.puntos_de_vida = self.puntos_de_vida - dano if\
			self.puntos_de_vida - dano > 0 else 0
		else:
			raise MonstruoDerrotado('El monstruo ya no tiene puntos de vida restantes.')

	def get_tipo1(self):
		"""Retorna el tipo1 del monstruo.
		"""
		return self.tipos[0]

	def get_tipo2(self):
		"""Retorna el tipo2 del monstruo. Retorna None si el Monstruo no tiene
		tipo2.
		"""
		return self.tipos[1]

	def get_puntos_de_vida(self):
		"""Retorna los puntos de vida restantes del monstruo.
		"""
		return self.puntos_de_vida

	def get_ataques_especiales(self):
		return self.ataques_especiales

class TipoDeMonstruo(object):
	"""Clase base de Tipos de monstruo. Define las interacciones entre
	distintos tipos. Un monstruo de TipoFuego es resistente a TipoAire y debil
	a TipoAgua, TipoTierra es resistente a TipoAgua y debil a TipoAire,
	TipoAire es resistente a TipoTierra y debil a TipoFuego, TipoAgua
	es debil a TipoTierra y resistente a TipoFuego.
	
	Argumentos:
	efectividad -- clase del tipo contra el que es fuerte TipoDeMonstruo.
	inefectividad -- clase del tipo contra el que debil TipoDeMonstruo.
	tipo -- cadena string que especifica el nombre del tipo.
	"""
	
	def __init__(self, efectividad, inefectividad, tipo):
		self.interacciones = (efectividad, inefectividad)
		self.tipo = tipo

	def atacar(self, tipo_def, ataqueEspecial):
		"""Calcula el daño causado a un monstruo del tipo indicado.
		
		Argumentos:
		tipo_def -- el tipo del monstruo defensor.
		ataqueEspecial -- valor booleano, indica si se utiliza un ataque especial.
		
		Retorna:
		El dano que puede causar el ataque.
		"""
		dano = 15 if ataqueEspecial else 10
		if type(tipo_def) == self.interacciones[0]:
			dano = dano * 1.2
		elif type(tipo_def) == self.interacciones[1]:
			dano = dano * 0.8
		return dano

	def __str__(self):
		return self.tipo

class TipoFuego(TipoDeMonstruo):
	"""Clase TipoFuego, representa el tipo de monstruo de fuego. Resiste aire,
	es debil a agua y causa dano adicional contra aire.
	"""

	def __init__(self):
		super().__init__(TipoAire, TipoAgua, 'Fuego')

class TipoTierra(TipoDeMonstruo):
	"""Clase TipoTierra, representa el tipo de monstruo de tierra. Resiste agua,
	es debil a aire y causa dano adicional contra agua.
	"""

	def __init__(self):
		super().__init__(TipoAgua, TipoAire, 'Tierra')

class TipoAire(TipoDeMonstruo):
	"""Clase TipoAire, representa el tipo de monstruo de aire. Resiste tierra,
	es debil a fuego y causa dano adicional contra tierra.
	"""

	def __init__(self):
		super().__init__(TipoTierra, TipoFuego, 'Aire')

class TipoAgua(TipoDeMonstruo):
	"""Clase TipoAgua, representa el tipo de monstruo de agua. Resiste fuego,
	es debil a tierra y causa dano adicional contra fuego.
	"""

	def __init__(self):
		super().__init__(TipoFuego, TipoTierra, 'Agua')

class MonstruoDerrotado(Exception):
	"""Excepcion levantada cuando se llama la funcion defender() desde un Monstruo
	sin puntos de vida restantes.
	"""
	pass

class SinAtaquesEspeciales(Exception):
	"""Excepcion levantada cuando se llama la funcion atacar() con ataque=2 o ataque=4 
	desde un Monstruo cuya funcion get_ataques_especiales() devuelve 0.
	"""
	pass

class TiposIguales(Exception):
	"""Excepcion levantada cuando se llama la funcion crear_monstruo() desde una instancia
	de BatallaDeMonstruos con argumentos del mismo.
	"""