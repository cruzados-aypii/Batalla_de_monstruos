import pickle
from sys import path
from sys import exit
import os
path.append(os.path.abspath(os.path.dirname(__file__)))

def save(partida, nombre):
	line_to_write_on = 1
	with open(path[-1] + '\\id_partida.txt', 'r') as ids:
		for line in ids:
			line_to_write_on += 1
	with open(path[-1] + '\\id_partida.txt', 'a') as file:
		file.write(str(line_to_write_on) + ' ' + nombre + '\n')
	pickle.dump(partida, open(path[-1] + '/' + nombre + '.txt', 'wb'))

def load(nombre):
	return pickle.load(open(path[-1] + '/' + nombre + '.txt', 'rb'))

def erase():
	with open(path[-1] + '\\id_partida.txt', 'r') as ids:
		for line in ids:
			try:
				os.remove(path[-1] + '\\' + line[2:-1] + '.txt')
			except FileNotFoundError:
				continue
	with open(path[-1] + '\\id_partida.txt', 'w') as ids:
		pass
		

def cargar_partida(juego):
	while True:
		print('Elija una de las partidas, o escribe "ayuda" para mas comandos\n')
		temp_juego = None
		with open(path[-1] + '/id_partida.txt', 'r') as partidas_guardadas:
			for line in partidas_guardadas:
				print(line)

		print('-----------------------------------------------------------------')
		comando = input('Numero de la partida a cargar: ')
		with open(path[-1] + '/id_partida.txt', 'r') as partidas_guardadas:
			for line in partidas_guardadas:
				if line.startswith(comando):
					temp_juego = load(line[2:-1])
					print(temp_juego.get_jugador_actual().producir_resumen() + '\n' +
					temp_juego.get_jugador_no_actual().producir_resumen())

		if temp_juego:
			print('-----------------------------------------------------------------')
			comando = input('¿Cargar esta partida?\n1. Si\n2. No\n')
			while True:
				if comando == '1':
					print('Cargando partida...')
					print('-----------------------------------------------------------------')
					return temp_juego
				elif comando == '2':
					print('Volviendo a la lista de partidas...')
					temp_juego = None
					break
				else:
					print('Comando invalido')
			continue

		elif comando == 'ayuda':
			print('Comandos adicionales:\ncancelar. Vuelve al menu anterior\ncerrar. Cierra el juego')
			input('Presiona enter para volver a la seleccion de partida')
			print('-----------------------------------------------------------------')
			continue

		elif comando == 'cancelar':
			print('Volviendo al menu anterior')
			print('-----------------------------------------------------------------')
			return juego

		elif comando == 'cerrar':
			print('Cerrando juego.')
			exit()

		print('Comando invalido')

